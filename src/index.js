import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import firebase from 'firebase';

const firebaseConfig = {
  apiKey: "AIzaSyBu8GC4ZKBm-QcW5AVhGB91vWrhu1ycEQI",
  authDomain: "cs297-data-vis-project.firebaseapp.com",
  databaseURL: "https://cs297-data-vis-project.firebaseio.com",
  projectId: "cs297-data-vis-project",
  storageBucket: "",
  messagingSenderId: "1095314160555",
  appId: "1:1095314160555:web:6377c1f8a0ad7e99"
};
firebase.initializeApp(firebaseConfig);

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

