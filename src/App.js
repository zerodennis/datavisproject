import React from 'react';
import './App.css';
import '../node_modules/react-vis/dist/style.css';
import {
  Navbar,
  Button,
} from '@blueprintjs/core';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import General from './components/General';
import Diabetes from './components/Diabetes';
import CommunicableDisease from './components/CommunicableDisease';
import Documentation from './components/Documentation';
import "normalize.css";
import "@blueprintjs/core/lib/css/blueprint.css";
import "@blueprintjs/icons/lib/css/blueprint-icons.css";

class App extends React.Component {
  render() {
    return (
      <Router>
        <div className="App">
          <header>
            <Navbar>
              <Navbar.Group align="left">
                  <Navbar.Heading>CS 297.3 - Data Visualization</Navbar.Heading>
                  <Navbar.Divider />
                  <Link to="/">
                    <Button className="bp3-minimal" icon="home" text="Home" />
                  </Link>
                  <Link to="/diabetes">
                    <Button className="bp3-minimal" icon="document" text="Diabetes" />
                  </Link>
                  <Link to="/communicable">
                    <Button className="bp3-minimal" icon="document" text="Communicable" />
                  </Link>
                  <Link to="/documentation">
                    <Button className="bp3-minimal" icon="document" text="Documentation" />
                  </Link>
              </Navbar.Group>
            </Navbar>
          </header>
          <Switch>
            <Route exact path='/' component={General} />
            <Route path='/diabetes' component={Diabetes} />
            <Route path='/communicable' component={CommunicableDisease} />
            <Route path='/documentation' component={Documentation} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
