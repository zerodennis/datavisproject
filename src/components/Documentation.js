import React from 'react';
import postman from '../assets/img/postman.png';
import colab from '../assets/img/colab.png';
import colab2 from '../assets/img/colab2.png';
const link = "https://3kllhk1ibq34qk6sp3bhtox1-wpengine.netdna-ssl.com/wp-content/uploads/2017/04/5-real-life-examples-of-beautiful-technical-documentation@3x-1560x760.png";

const Documentation = ({ navigation }) => {
  return (
    <div>
      <header>

      </header>
      <body>
        <div
          id="header"
          style={{
            height: '20rem',
            width: '100%'
          }}
        >
          <img
            src={link}
            style={styles.headerImage}
            alt="not found"
          />
        </div>
        <div className="container" style={styles.container}>
          <div style={styles.textBlock}>
            <h1>Problem</h1>
            <p style={{ fontSize: 16 }}>
              The utilization of digital technology to aid in medical practice in the Philippines has seen a rise over the years. One of the ways this has been done is through the implementation of Electronic Medical Records (EMR) in order to record, and store patient data, and the Philippine government has taken steps towards the fulfillment of this in more public spaces since around 2013.<sup>1</sup> Because of this, patient data has become more easily shared and communicated between clinics, and even between patients.
            </p>
            <p style={{ fontSize: 16 }}>
              Due to the ease of access to large volumes of patient data, we are able to easily, and more conveniently study patient data so as to understand the trends and statistics regarding certain types of important information gained from many patients. These could include, but are not limited to, the volume of prescriptions in an area or the amount of people diagnosed with a certain illness or disease. Understanding this data lends itself to making data-driven decisions.
            </p>
          </div>
          <div style={styles.textBlock}>
          <h1>Research Questions</h1>
          <p style={{ fontSize: 16 }}>
            <ol>
              <li>What are the sicknesses/diseases/anomalies that are most diagnosed in the Philippines?</li>
              <li>What is the distribution of the diagnosed illnesses throughout the Philippines?</li>
              <li>What are the most prescribed drugs in the Philippines?</li>
            </ol>
          </p>
          <p style={{ fontSize: 16 }}>
            As a "subsection" of my project, I took an interest in examining data on cases of diabetes, and some communicable diseases in the Philippines namely: Dengue, measles, and typhoid. The research questions for these is as follows:
            <ol>
              <li>What is the volume of diagnosed cases of dengue, measles, and typhoid in the Philippines, and how do they compare to each other?</li>
              <li>What is the volume and distribution of diagnosed cases of diabetes in the Philippines?</li>
            </ol>
          </p>
          </div>
          <div style={styles.textBlock}>
            <h1>Dataset</h1>
            <h3>Acquisition</h3>
            <p style={{ fontSize: 16 }}>
              The team behind the development of an EMR system gave me access to their API in order for me to access anonymized, yet located patient data. I used Postman to gather data from API endpoints on their side. In total, I got approximately 200000 rows of data, encompassing health records from January 2018 - June 2019.
            </p>
            <img
              src={postman}
              alt="not found"
              style={{
                width: 600,
                height: 600,
                objectFit: 'contain',
                alignSelf:'center'
              }}
            />
            <h3>Preparation and Cleaning</h3>
            <p style={{ fontSize: 16 }}>
              The data that I got was separated by month, and was in JSON format. For proper processing, they would have to be converted to CSV, a more "queryable" format. Thankfully, Spark has methods for this, and thankfully, it is easy to create a Spark environment with PySpark on Google Colaboratory, which is where I did all of my processing and cleaning. The steps involved are as follows:

              <ol>
                <li><strong>Install Spark - </strong>The environment used to process large amounts of data quickly, and in parallel</li>
                <li><strong>Examine the data - </strong>I took a look at all the available features in the dataset, and decided which ones would only be needed. Some features that I deemed unneeded were dropped, images depicting the cleaning processes and the resulting schema of the data after dropping a few features are shown below.
                </li>
                <li><strong>Get only the data that include an ICD 10 code as a feature value - </strong>ICD is an international standard used by the DOH since the early 2000s to record, share, and communicate diseases<sub>2</sub> If an instance contains an ICD 10 value, I took it to mean that the diagnosis has been 100% confirmed. The source I took this from also informed me that the clinic only reports to the DOH, cases that have ICD 10 codes attached to them.</li>
                <li><strong>Combine the data for all months</strong></li>
                <li><strong>Remove all null or empty values</strong></li>
                <li><strong>Sort by date</strong></li>
                <li><strong>Convert back to JSON - </strong>once the data has been cleaned and processed, the results are then converted back into JSON to be used with JavaScript</li>
              </ol>

              The Colaboratory notebook can be found here: <a href="https://colab.research.google.com/drive/1eeUc2dsetI2CJNGWqR_1d0gAiY331yjL?authuser=1">Colab Notebook</a>
            </p>
            <div style={{
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
              <img
                src={colab}
                alt="not found"
                style={{
                  width: 800,
                  height: 600,
                  objectFit: 'contain',
                  flex: 1,
                  display: 'flex',
                }}
              />
              <img
                src={colab2}
                alt="not found"
                style={{
                  width: 800,
                  height: 600,
                  objectFit: 'contain',
                  flex: 1,
                  display: 'flex',
                }}
              />
            </div>
          </div>
          <div style={styles.textBlock}>
            <h1>Data Visualization Process</h1>
            <p style={{ fontSize: 16 }}>
              As I am a software developer well-versed in using Facebook's React platform with ReactJS for the web and React Native for mobile systems, I decided to use ReactJS to display my data visualizations, thus the use of data in JSON format.
            </p>
            <p style={{ fontSize: 16 }}>
              The data visualization libraries that I used are:
                <ol>
                  <li><a href="https://uber.github.io/react-vis/">react-vis</a></li>
                  <li><a href="https://uber.github.io/react-map-gl/#/">react-maps-gl</a></li>
                </ol>
            </p>
            <p>
              For easy parsing of data I used:
              <ol>
                <li><a href="https://lodash.com">lodash</a></li>
                <li><a href="https://papaparse.com">papa parse</a></li>
              </ol>
            </p>
          </div>
          <div style={styles.textBlock}>
            <h1>Findings & Evaluation</h1>
            <p style={{ fontSize: 16 }}>
              The findings and evaluation for the study can be found on their respective web pages. Simply click on a link on the navigation bar above to see them.
            </p>
          </div>
          <div style={styles.textBlock}>
          <h1>Limitations and Issues</h1>
            <h3>The data</h3>
            <p style={{ fontSize: 16 }}>
              Though I have used "in the Philippines" as my scope for the project, taking a look at the data I have procured reveals that I only have patient records from regions 6, 7, 1, 13, 5, and 4. This is because these are the locations where the system is being used.
            </p>
            <h3>The visualization</h3>
            <p style={{ fontSize: 16 }}>
              Since the data has to be anonymized, and "located" with this restriction in place, the location data does not provide coordinates in latitude and longitude format, which most mapping and navigation libraries require. The data provides it in Philippine Standard Geographic Code (PSGC) format which is used by the Philippine Statistics Authority.
            </p>
            <p style={{ fontSize: 16 }}>
              Currently there is no mapping of the proprietary PSGC to latitude and longitude (that I know of) yet, thus displaying the locations described by the data on the map was challenging, and tedious.
            </p>
          </div>
          <div style={styles.textBlock}>
          <h1>Sources</h1>
            <div>
              <ol>
                <li>http://ehealth.doh.gov.ph/index.php/phie/overview</li>
                <li>https://www.doh.gov.ph/node/1220</li>
              </ol>
            </div>
          </div>
        </div>
      </body>
    </div>
  );
};

const styles = {
  constainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  chartBlock: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '2rem'
  },
  textBlock: {
    float: 'none',
    marginLeft: '22rem',
    marginRight: '22rem',
    marginTop: '4rem',
    lineHeight: 1.6,
    flexDirection: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
  },
  headerImage: {
    'minWidth': '100%',
    height: '100%',
    objectFit: 'cover'
  },
}

export default Documentation;
