import React from 'react';
import '../App.css';
import '../../node_modules/react-vis/dist/style.css';
import {
  XYPlot,
  XAxis,
  YAxis,
  VerticalBarSeries,
  LabelSeries,
  HorizontalBarSeries,
} from 'react-vis';
import * as Papa from 'papaparse';
import _ from 'lodash';
import MapGL, {
  NavigationControl,
  Marker,
  Popup,
} from 'react-map-gl';
import Immutable from 'immutable';
import location from '../assets/icons/location.png';
import lat_long_map from '../assets/data/lat_long_map.json';
import national_data from '../assets/data/national.json';
import meds_national from '../assets/data/meds_national.json';
import meds from '../assets/data/meds.json';
import healthdata from '../assets/data/healthdata.json';
import * as moment from 'moment';
import "normalize.css";
import "@blueprintjs/core/lib/css/blueprint.css";
import "@blueprintjs/icons/lib/css/blueprint-icons.css";

// const MAP_BOX_TOKEN = 'sk.eyJ1IjoiemVyb2Rlbm5pcyIsImEiOiJjanhvbW0xemMwN3d6M29wOXZoMGliZm01In0.s5hRoKBELYUt7_qFNptyUQ';
const TOKEN = 'pk.eyJ1IjoiemVyb2Rlbm5pcyIsImEiOiJjanhvbWR2d2EwNDloM2NsMzhtemR4N3N4In0.4pBv8E4B15TAAq-Y0YEvng';
const INITIAL_STATE = {
  data: null,
  loading: false,
  icd10: [],
  viewport: {
    latitude: 11.0050,
    longitude: 122.5373,
    zoom: 5.0,
    bearing: 0,
    pitch: 0,
    width: '80%',
    height: 500,
  },
  popupInfo: null,
}

class General extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ...INITIAL_STATE,
    };
    this.onSubmitCSV = this.onSubmitCSV.bind(this);
    this.processData = this.processData.bind(this);
    this.getICD10Data = this.getICD10Data.bind(this);
    this.getPrescriptionSummary = this.getPrescriptionSummary.bind(this);
    this.getDiabetesSummary = this.getDiabetesSummary.bind(this);
    document.title = "Data Vis App"
  }

  componentDidMount = () => {
    // this.getDiabetesSummary(healthdata);
    // let data = new Blob ([JSON.stringify(this.getDiabetesSummary(healthdata))], { type: 'text/plain'});
    // var a = window.document.createElement("a");
    // a.href = window.URL.createObjectURL(data, {type: "text/plain"});
    // a.download = "typhoid_location.json";
    // document.body.appendChild(a);
    // a.click();  // IE: "Access is denied"; see: https://connect.microsoft.com/IE/feedback/details/797361/ie-10-treats-blob-url-as-cross-origin-and-denies-access
    // document.body.removeChild(a);
  }

  getDiabetesSummary = input => {
    _.forEach(input, value => {
      value["created_at"] = moment(value.created_at).format('MMMM YYYY');
    });

    let diabArr = input.reduce((obj, v) => {
      if (v.diagnosislist_id.toLowerCase().includes("influenza")) {
        obj[v.created_at] = (obj[v.created_at] || 0) + 1;
        // obj[v.facility_barangay] = (obj[v.facility_barangay] || 0) + 1;
      }
      return obj;
    }, {});

    let dates = Object.keys(diabArr);
    let diabRes = [];
    _.forEach(dates, (value) => {
      diabRes.push({
        code: value,
        count: diabArr[value]
      })
    });

    console.log(diabRes)
    return diabRes;
  }

  onSubmitCSV = event => {
    const { data } = this.state;
    event.preventDefault();
  }

  onUploadCSV = event => {
    this.processData(event.target.files[0])
    event.preventDefault();
  }

  processData = file => {
    Papa.parse(file, {
      header: true,
      skipEmptyLines: true,
      complete: results => {
        // let data = new Blob ([_.forEach(results.data, value => JSON.stringify(value))], { type: 'text/plain'});
        // var a = window.document.createElement("a");
        // a.href = window.URL.createObjectURL(data, {type: "text/plain"});
        // a.download = "filename.json";
        // document.body.appendChild(a);
        // a.click();  // IE: "Access is denied"; see: https://connect.microsoft.com/IE/feedback/details/797361/ie-10-treats-blob-url-as-cross-origin-and-denies-access
        // document.body.removeChild(a);
        // console.log(results)
      }
    })
  }

  getDiabSummary = value => {

  }

  getPrescriptionSummary = value => {
    let termsList = meds.results;
    termsList = _.map(termsList, (value, index) => {
      value = {
        ...value,
        instances : 0,
      };
      return value;
    });
    let data = value;
    console.log(termsList)
    data.forEach(value => {
      _.forEach(termsList, med => {
        if (value.prescription.includes(med.term)) {
          med.instances +=1
        }
      })
    });
    termsList = _.sortBy(termsList, ['instances']);
    console.log(termsList);
  }

  getICD10Data = (value) => {
    let icdArr = value.reduce((obj, v) => {
      obj[v.facility_barangay] = (obj[v.facility_barangay] || 0) + 1;
      return obj;
    }, {});

    let icdKeyArray = Object.keys(icdArr);
    let icdRes = [];
    _.forEach(icdKeyArray, (value) => {
      icdRes.push({
        code: value,
        count: icdArr[value]
      })
    });

    // _.forEach(icdRes, (value, key) => {
    //   _.forEach(icd10_dict, (v, k) => {
    //     if (icdRes[key].code.split(".").join("") == v.code)
    //       icdRes[key]['desc'] = v.desc;
    //   })
    // })

    icdRes = _.sortBy(icdRes, ['count']);


    // pairs.sort(function (a, b) {
    //   return a[1] - b[1];
    // });
    console.log(icdRes);
  }

  renderPopup(){
    return this.state.popupInfo && (
      <Popup tipSize={5}
        anchor="bottom-right"
        longitude={this.state.popupInfo.longitude}
        latitude={this.state.popupInfo.latitude}
        onClose={() => this.setState({popupInfo: null})}
        closeOnClick={true}>
        <p>{this.state.popupInfo.message}</p>
      </Popup>
    )
  }

  render() {
    let meds_national_data = [];
    _.forEach(meds_national, value => {
      meds_national_data.push({ "x": value.instances, "y": value.term })
    }, meds_national_data)
    const markers = _.map(lat_long_map, (value, index) => {
      return (
          <Marker
            latitude={value.latitude}
            longitude={value.longitude}
            key={value.code}
          >
            <img
              src={location}
              width={32}
              height={32}
              alt="not found"
              onClick={() => {
                this.setState({
                  popupInfo: {
                    message: `${value.count} Cases`,
                    latitude: value.latitude,
                    longitude: value.longitude,
                  }
                })
              }}
            />
          </Marker>
        )
    }, [])
    const chartWidth = 800;
    const chartHeight = 600;
    const { viewport } = this.state;
    return (
      <div>
      <div
      id="header"
      style={{
        height: '20rem',
        width: '100%'
      }}
    >
      <img
        src={`https://images.unsplash.com/photo-1559019727-963e0a4eab74?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2390&q=80`}
        style={styles.headerImage}
        alt="not found"
      />
    </div>
    {
      // <input
      //   type="file"
      //   id="upload"
      //   name="data"
      //   accept=".csv"
      //   ref={input => {
      //     this.filesInput = input;
      //   }}
      //   onChange={this.onUploadCSV}
      // />
    }
    <div
      style={styles.textContainer}
    >
    <h1>
      Visualizing Health Records - A Project for CS 297.3: Data Visualization
    </h1>
    <h3>
      <i>Note: This is a work in progress!</i>
    </h3>
    <p style={styles.articleText}>
      For this study, I have been given access to anonymized, yet located, electronic medical records. It is an online browser-based and mobile-based application that caters to the data management needs of doctos, nurses, and other related professionals practicing in the health field. For the visualizations presented below, the data used is from the January 2019 - June 2019.
    </p>
    <p style={styles.articleText}>
      Each "row" in the dataset represents one or more consultations made by a patient. Only those with an ICD 10 value in the row was considered for processing. This is because only those with an ICD 10 value can be reported to the DOH, and I took this to mean that a case for that diagnosis is "confirmed" (i.e. not an impression, but a confirmed case). This cut my dataset from around 50000 records to around 30000.
    </p>
    </div>
    <div
    style={styles.textContainer}
  >
  <h1>
    Distribution of Cases
  </h1>
  <p style={styles.articleText}>
    To maintain privacy in the dataset, the location data was stored as a "Philippine Standard Geographic Code" or PSGC code, a 9-digit code that the Philippine Statistics Authority (PSA) uses. Currently ther eis no database providing a mapping of these codes to coordinate values (Latitude & Longitude), but there does exist a mapping of the code to the location that the code represents. I then looked up the latitude and longitude and made a mapping of the location to the code.
  </p>
  <p style={styles.articleText}>
    This map represents the distribution of the total number of cases <i>per region.</i>
  </p>
  </div>
    <div
      style={{
        width: '100%',
        height: 'auto',
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'column'
      }}
    >
      <MapGL
        {...viewport}
        mapStyle="mapbox://styles/mapbox/dark-v9"
        mapboxApiAccessToken={TOKEN}
        onViewportChange={viewport => this.setState({viewport})}
      >
        {markers}
        {this.renderPopup()}
        <div className="nav" style={styles.navStyle}>
          <NavigationControl/>
        </div>
      </MapGL>
      <p>Click on the icons to view more information</p>
    </div>
    <div
      style={styles.textContainer}
    >
      <h1>
        The Top Diagnoses
      </h1>
      <p style={styles.articleText}>
        Across all the locations that use the system, these are the cases with the highest volume of patients, the ICD 10 code in the patient record was matched against a database containing a mapping of the ICD 10 value to the medical term or operation that the code represents. As we can see from the chart, hypertension is the most prevalent sickness.
      </p>
    </div>
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '2rem'
      }}
    >
      <XYPlot
        xType="ordinal"
        width={chartWidth}
        height={chartHeight}
        margin={{
          left: 100,
          top: 40,
        }}
      >
      <XAxis
        style={{
          text: {
            wordBreak: 'break-word',
            whiteSpace: 'pre'
          }
        }}
        tickFormat={function myFormatter(t, i) {
          return (
            <tspan>
              <tspan x="0" dy="1em" textLength={60} lengthAdjust="spacingAndGlyphs">{t}</tspan>
            </tspan>
            );
        }}
      />
      <YAxis />
      <VerticalBarSeries
          data={national_data}
      />
      <LabelSeries
        data={national_data.map(obj => {
            return { ...obj, label: obj.y.toString() }
        })}
        labelAnchorX="middle"
        labelAnchorY="text-after-edge"
      />
      </XYPlot>
    </div>
    <div
    style={styles.textContainer}
    >
    <h1>
      The Most Prescribed Drugs
    </h1>
      <p style={styles.articleText}>
        Every patient record may contain a prescription given by the doctor that checked up on them. The prescription given was checked to see if it includes the generic name of a medicine in a dataset of generic medicine names taken from the OpenFDA API.
      </p>
    </div>
    <div
    style={{
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: '2rem',
      alignSelf: 'stretch'
    }}
  >
    <XYPlot
      yType="ordinal"
      width={chartWidth}
      height={chartHeight}
      margin={{
        right: 100,
        top: 40,
        left: 120,
      }}
    >
    <YAxis />
    <HorizontalBarSeries
        data={meds_national_data}
        color="#8BC43F"
    />
    <LabelSeries
      data={meds_national_data.map(obj => {
          return { ...obj, label: obj.x.toString() }
      })}
      labelAnchorY="middle"
      labelAnchorX="text-after-edge"
    />
    </XYPlot>
    </div>
    <div
    style={styles.textContainer}
    >
    <h1>
      Demographics
    </h1>
      <p style={styles.articleText}>
        <strong><i>Coming Soon</i></strong>
      </p>
    </div>
    <div
    style={styles.textContainer}
    >
    <h1>
      Technical Implementation and Limitations
    </h1>
      <h3>Limitations</h3>
      <p style={styles.articleText}>
        The data used is only from Regions 6, 7, 1, 13, 5, and 4, of the Philippines, this is because these must be the only locations where the system is being used.
      </p>
      <p style={styles.articleText}>
        No list of generic medicine names widely used in the Philippines has been used yet, so to match the prescription given in one record, a list was taken from the OpenFDA API, limited to around 5000 terms.
      </p>
      <h3>Implementation</h3>
      <p style={styles.articleText}>
        The data was acquired using the system's API, cleaned using PySpark running on Apache Spark installed on Google Colaboratory, then the Visualization part as well as this webpage was made using ReactJS, MapBox, and React-Vis
      </p>
    </div>
      </div>
    );
  }
}

const styles = {
  headerImage: {
    'minWidth': '100%',
    height: '100%',
    objectFit: 'cover'
  },
  textContainer: {
    float: 'none',
    marginLeft: '22rem',
    marginRight: '22rem',
    marginTop: '4rem',
    lineHeight: 1.6
  },
  articleText: {
    fontSize: 16,
  },
  navStyle: {
    position: 'absolute',
    top: 0,
    left: 0,
    padding: '10px'
  }
}

export default General;
