import React, { Component } from 'react';
import '../../node_modules/react-vis/dist/style.css';
import {
  XYPlot,
  XAxis,
  YAxis,
  HorizontalGridLines,
  VerticalGridLines,
  VerticalBarSeries,
  LineMarkSeries,
  DiscreteColorLegend,
  LabelSeries,
  Hint,
} from 'react-vis';
import "normalize.css";
import "@blueprintjs/core/lib/css/blueprint.css";
import "@blueprintjs/icons/lib/css/blueprint-icons.css";
import _ from 'lodash';
import dengueMonthly from '../assets/data/dengue_monthly';
import measlesMonthly from '../assets/data/measles_monthly';
import typhoidMonthly from '../assets/data/typhoid_monthly';
import total from '../assets/data/communicable_diseases_total';

const dengueMonthlyData = [];
_.forEach(dengueMonthly, value => {
  dengueMonthlyData.push({ "x": new Date(value.code), "y": value.count })
}, dengueMonthlyData);
const measlesMonthlyData = [];
_.forEach(measlesMonthly, value => {
  measlesMonthlyData.push({ "x": new Date(value.code), "y": value.count })
}, measlesMonthlyData);
const typhoidMonthlyData = [];
_.forEach(typhoidMonthly, value => {
  typhoidMonthlyData.push({ "x": new Date(value.code), "y": value.count })
}, typhoidMonthlyData);

class CommunicableDisease extends Component {
  constructor(props) {
    super(props);
    this.state = {
      viewport: {
        latitude: 11.0050,
        longitude: 122.5373,
        zoom: 5.0,
        bearing: 0,
        pitch: 0,
        width: '80%',
        height: 500,
      },
      popupInfo: null,
      value: false,
    };
  }

  render() {
    return (
      <div>
      <div
      id="header"
      style={{
        height: '20rem',
        width: '100%'
      }}
    >
      <img
        src={` https://d2r55xnwy6nx47.cloudfront.net/uploads/2019/04/MarineVirus_1200x630-Social1.jpg`}
        style={styles.headerImage}
        alt="not found"
      />
    </div>
        <div className="container" style={styles.container}>
          <div style={styles.textBlock}>
            <h1>Communicable Diseases in the Philippines</h1>
            <p style={{ fontSize: 16 }}>
              Communicable, or infectious diseases, are diseases that can be passed on from one organism to another. In other words, they are diseases that require a <i>vector.</i> Some diseases are transmitted by insect bites, and some are transmitted by infected water, among other things. Today we will be looking at three of the most common communicable diseases in the Philippines: dengue, measles, and typhoid.
            </p>
          </div>
          <div style={styles.chartBlock}>
          <XYPlot
            xType="time"
            width={1200}
            height={600}
          >
          <DiscreteColorLegend
            orientation="horizontal"
            height={300}
            width={200}
            items={['Dengue', 'Measles', 'Typhoid']}
          />
            <XAxis
              title="Month"
            />
            <YAxis title="Reported Cases" />
            <VerticalGridLines />
            <HorizontalGridLines />
            <LineMarkSeries
              data={dengueMonthlyData}
              onValueClick={value => this.setState({ value })}
            />
            <LineMarkSeries
              data={measlesMonthlyData}
              onValueClick={value => this.setState({ value })}
            />
            <LineMarkSeries
              data={typhoidMonthlyData}
              onValueClick={value => this.setState({ value })}
            />
            {this.state.value ? <Hint value={this.state.value} /> : null}
          </XYPlot>
          </div>
          <div style={styles.textBlock}>
            <h1>Dengue, Measles, and Typhoid</h1>
            <br></br>
            <h2>Dengue</h2>
            <p style={{ fontSize: 16 }}>
              From the data I have processed, we can see that dengue would seem to be the most prevalent infectious disease during the time period from 2018 up to June 2019. We can gather that there may be a seasonality to dengue as the number of cases seem to spike from June to September. This is most probably due to the Philippines' rainy season being somewhere from June to October. During the first 20 weeks of 2019, there have been 77040 suspected cases of dengue in the Philippines,<sup>3</sup> and based on historical data, the number is set to increase as we go into the rainy season.<sup>4</sup>
            </p>
            <h2>Measles</h2>
            <p style={{ fontSize: 16 }}>
              Measles is a disease that humans can gain immunity to by taking a measles vaccine. It is a disease that since the year 2000, has been declared as eliminated in the Americas. However, in some parts of the world, cases or even outbreaks of measles may still occur. In fact, just recently a measles outbreak was reported to have broken out in the Philippines in February 2019.<sup>1</sup> This is reflected in the time series chart showing a drastic increase in the number of reported measles cases from February 2019 to March 2019, likely due to the <strong>almost</strong> negative sentiment that some of the public have towards vaccination. According to the WHO, in 2019, measles has seen a case increase of 30% globally, and although hesitance and lack of confidence in vaccination may contribute to this, it is not the sole cause of the "measles resurgence<sup>2</sup>"
            </p>
            <h2>Typhoid</h2>
            <p style={{ fontSize: 16 }}>
              The infectious disease with the least number of cases reported in our dataset is Typhoid. This is a disease that can only spread in environments where human fecal matter can come into contact with food or drinking water, thus proper sanitation and oral hygiene is needed to avoid it. Typhoid may not be widespread in the Philippines, and that may be due to the quality of water or infrastructure improving in the locations we have recorded cases in.
            </p>
          </div>
          <div style={styles.chartBlock}>
            <XYPlot
              xType="ordinal"
              width={600}
              height={400}
              margin={{
                left: 100,
                top: 40,
              }}
            >
              <XAxis />
              <YAxis />
              <VerticalGridLines />
              <HorizontalGridLines />
              <LabelSeries
                data={total.map(obj => {
                    return { ...obj, label: obj.y.toString() }
                })}
                labelAnchorX="middle"
                labelAnchorY="text-after-edge"
              />
              <VerticalBarSeries
                data={total}
              />
            </XYPlot>
          </div>
          <div style={styles.textBlock}>
          <h1>Sources</h1>
            <div>
              <ol>
                <li>https://www.doh.gov.ph/node/16647</li>
                <li>https://www.who.int/emergencies/ten-threats-to-global-health-in-2019</li>
                <li>https://www.who.int/westernpacific/news/detail/11-06-2019-dengue-increase-likely-during-rainy-season-who-warns</li>
                <li>https://www.doh.gov.ph/node/17435</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    )
  }
};

const styles = {
  constainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  chartBlock: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '2rem'
  },
  textBlock: {
    float: 'none',
    marginLeft: '22rem',
    marginRight: '22rem',
    marginTop: '4rem',
    lineHeight: 1.6
  },
  headerImage: {
    'minWidth': '100%',
    height: '100%',
    objectFit: 'cover'
  },
}

export default CommunicableDisease;

