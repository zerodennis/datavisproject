import React, { Component } from 'react';
import '../../node_modules/react-vis/dist/style.css';
import {
  XYPlot,
  XAxis,
  YAxis,
  HorizontalGridLines,
  VerticalGridLines,
  LineMarkSeries,
  Hint,
} from 'react-vis';
import MapGL, {
  Marker,
  Popup,
} from 'react-map-gl';
import "normalize.css";
import "@blueprintjs/core/lib/css/blueprint.css";
import "@blueprintjs/icons/lib/css/blueprint-icons.css";
import _ from 'lodash';
import diabetesMonthly from '../assets/data/diabetes_monthly';
import diabetesLocation from '../assets/data/diabetes_location';
import location from '../assets/icons/location.png';

const diabetes_monthly_data = [];
_.forEach(diabetesMonthly, value => {
  diabetes_monthly_data.push({ "x": new Date(value.code), "y": value.count })
}, diabetes_monthly_data);

const TOKEN = 'pk.eyJ1IjoiemVyb2Rlbm5pcyIsImEiOiJjanhvbWR2d2EwNDloM2NsMzhtemR4N3N4In0.4pBv8E4B15TAAq-Y0YEvng';
const link = 'https://i0.wp.com/post.greatist.com/wp-content/uploads/sites/2/2019/06/fruits-for-diabetics-header-3-1296x728.jpg?w=1155';
class Diabetes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      datapoints: diabetes_monthly_data,
      viewport: {
        latitude: 11.0050,
        longitude: 122.5373,
        zoom: 5.0,
        bearing: 0,
        pitch: 0,
        width: '80%',
        height: 500,
      },
      popupInfo: null,
      value: false,
    };
    this.renderPopup = this.renderPopup.bind(this);
  }

  renderPopup(){
    return this.state.popupInfo && (
      <Popup tipSize={5}
        anchor="bottom-right"
        longitude={this.state.popupInfo.longitude}
        latitude={this.state.popupInfo.latitude}
        onClose={() => this.setState({popupInfo: null})}
        closeOnClick={true}>
        <p>{this.state.popupInfo.message}</p>
      </Popup>
    )
  }
  render() {
    const markers = _.map(diabetesLocation, (value, index) => {
      return (
          <Marker
            latitude={value.lat}
            longitude={value.long}
            key={value.code}
          >
            <img
              src={location}
              width={32}
              height={32}
              alt="not found"
              onMouseOver={() => {
                this.setState({
                  popupInfo: {
                    message: `${value.count} Cases`,
                    latitude: value.lat,
                    longitude: value.long,
                  }
                })
              }}
            />
          </Marker>
        )
    }, [])
    const { viewport } = this.state;
    return (
      <div>
        <header>

        </header>
        <div
          id="header"
          style={{
            height: '20rem',
            width: '100%'
          }}
        >
          <img
            src={link}
            style={styles.headerImage}
            alt="not found"
          />
        </div>
        <div className="container" style={styles.container}>
          <div style={styles.textBlock}>
            <h1>Something about Diabetes</h1>
            <p style={{ fontSize: 16 }}>
              Diabetes is one of the leading causes of mortality in the world, and according to the WHO, "prevalence is increasing worldwide, particularly in low- and middle-income countries<sup>1</sup>" In 2016, diabetes was the cause of 1.6 million deaths.<sup>1</sup> This is a look into diabetes in the Philippines given the data that we have.
            </p>
            <p style={{ fontSize: 16 }}>
              For this dataset, we only take into consideration cases of diabetes that have been diagnosed and reported to the Department of Health (DOH).
            </p>
          </div>
          <div style={styles.chartBlock}>
            <XYPlot
              xType="time"
              width={1200}
              height={600}
            >
              <XAxis
                title="Month"
              />
              <YAxis title="Reported Cases" />
              <VerticalGridLines />
              <HorizontalGridLines />
              <LineMarkSeries
                data={diabetes_monthly_data}
                onNearestXY={value => this.setState({ value }, () => console.log(this.state))}
              />
              {this.state.value ? <Hint value={this.state.value} /> : null}
            </XYPlot>
          </div>
          <div style={styles.textBlock}>
          <h1>A General Upward Trend</h1>
          <p style={{ fontSize: 16 }}>
            As seen in the figure above, there is a general upward trend in the cases of diabetes being reported to the DOH, except interestingly in December of 2018 where there seems to have been the lowest number of cases from 2018-2019.

            One of the leading causes of diabetes is diet and lifestyle. High amounts of sugar and saturated fats as well as the use of tobacco all contribute to an individual's being at risk of diabetes. The Philippine government saw fit to increase the tax on sweetened beverages, and tobacco, and framed it "as a preventive health measure that addressed features of the food market associated with increased rates of obesity and diabetes.<sup>3</sup>"
          </p>
          <p style={{ fontSize: 16 }}>
            The tax on sweetened beverages and tobacco was incorporated into the Tax Reform for Acceleration and Inclusion (TRAIN) Bill.
            A study conducted by Onagan et. al found that since January 2018, among all the products taxable by this bill, non-alcoholic sweetened beverages experienced the biggest increase in price, going as high as a 20% increase, while sales of these kinds of beverages in <i>Sari-sari</i> stores fell by an average of 8.7% in January 2018.<sup>3</sup> However, despite increases in price, beverage companies continued to drive sales by using "enhanced" marketing and offering their products in variants of smaller portions.<sup>3</sup>
          </p>
          <p style={{ fontSize: 16 }}>
            This may be the reason why we can still see an upward trend in the number of cases of diabetes recorded, however, I posit that more study and monitoring needs to be done on the effect of the tax on sweetened beverages on the illnesses (obesity and diabetes among others) they were made to guard against.
          </p>
          </div>
          <div style={styles.chartBlock}>
            <MapGL
              {...viewport}
              mapStyle="mapbox://styles/mapbox/dark-v9"
              mapboxApiAccessToken={TOKEN}
              onViewportChange={viewport => this.setState({viewport})}
            >
              {markers}
              {this.renderPopup()}
            </MapGL>
          </div>
          <div style={styles.textBlock}>
            <h1>Rural Areas See High Volume of Diabetes</h1>
            <p style={{ fontSize: 16 }}>
              Our results may contain only approximately 1000+ reported cases of diabetes, but those are only ones that have an ICD 10 code attached to the record. If they hadn't been removed that number would have dipped into the 3000-5000 mark.
            </p>
            <p style={{ fontSize: 16 }}>
              Still, we can see from our map above that Region 6, the Western Visayas region had the most number of diabetes cases that could be reported to the DOH, with one clinic in a barangay having as much as 383 cases of diabetes.
            </p>
          </div>
          <div style={styles.textBlock}>
          <h1>Sources</h1>
            <div>
              <ol>
                <li>https://www.who.int/news-room/fact-sheets/detail/diabetes</li>
                <li>https://www.doh.gov.ph/node/1220</li>
                <li>https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6357562/</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    )
  }
};

const styles = {
  constainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  chartBlock: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '2rem'
  },
  textBlock: {
    float: 'none',
    marginLeft: '22rem',
    marginRight: '22rem',
    marginTop: '4rem',
    lineHeight: 1.6
  },
  headerImage: {
    'minWidth': '100%',
    height: '100%',
    objectFit: 'cover'
  },
}

export default Diabetes;
