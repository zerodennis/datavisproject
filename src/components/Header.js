import React from 'react';
import {
  Navbar,
  Button,
} from '@blueprintjs/core';

const Header = () => {
  return (
    <Navbar>
      <Navbar.Group align="left">
        <Navbar.Heading>CS 297.3 - Data Visualization</Navbar.Heading>
        <Navbar.Divider />
          <Button className="bp3-minimal" icon="home" text="Home" />
          <Button className="bp3-minimal" icon="document" text="Diabetes" />
          <Button className="bp3-minimal" icon="document" text="Vectors" />
      </Navbar.Group>
    </Navbar>
  )
}

export default Header;
